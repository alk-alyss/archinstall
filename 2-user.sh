#!/bin/sh

cd ~
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si --noconfirm
cd ..
rm -rf yay

# Install packages
yay --noconfirm -S - < $HOME/archinstall/packagelist.txt

if [[ ${DESKTOP} ]]; then
	yay --noconfirm -S - < $HOME/archinstall/desktop-packages.txt
elif [[ ${LAPTOP} ]]; then
	yay --noconfirm -S - < $HOME/archinstall/laptop-packages.txt
fi

git clone --bare https://gitlab.com/alk-alyss/dotfiles $HOME/.files

alias dots='/usr/bin/git --git-dir=$HOME/.files/ --work-tree=$HOME'
dots config --local status.showUntrackedFiles no

mv /home/alkinoos/.bash_logout /home/alkinoos/.bash_logout.bak
mv /home/alkinoos/.bash_profile /home/alkinoos/.bash_profile.bak
mv /home/alkinoos/.bashrc /home/alkinoos/.bashrc.bak

dots checkout

xdg-mime default org.pwmt.zathura.desktop application/pdf
