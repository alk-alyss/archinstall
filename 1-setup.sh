#!/bin/bash

# Import configuration file
source /root/archinstall/setup.conf

nc=$(grep -c processor /proc/cpuinfo)
echo -e "Change makeflags and compression settings for '$nc' number of cores"
sed -i "s/#MAKEFLAGS=\"-j2\"/MAKEFLAGS=\"-j$nc\"/g" /etc/makepkg.conf
sed -i "s/COMPRESSXZ=(xz -c -z -)/COMPRESSXZ=(xz -c -T $nc -z -)/g" /etc/makepkg.conf

echo "Generate locale for en_US and el_GR, set language to en_US"
sed -i 's/^#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen
sed -i 's/^#el_GR.UTF-8 UTF-8/el_GR.UTF-8 UTF-8/' /etc/locale.gen
locale-gen
localectl --no-ask-password set-locale LANG="en_US.UTF-8" LC_TIME="en_US.UTF-8"

echo "Set keymap"
localectl --no-ask-password set-keymap ${KEYMAP}

echo "Setup timezone"
timedatectl --no-ask-password set-timezone ${TIMEZONE}
timedatectl --no-ask-password set-ntp 1

# Give no password sudo rights to wheel group
sed -i 's/^# %wheel ALL=(ALL:ALL) NOPASSWD: ALL/%wheel ALL=(ALL:ALL) NOPASSWD: ALL/' /etc/sudoers

# Enable parallel downloading multilib and color
sed -i 's/#Para/Para/' /etc/pacman.conf
sed -i 's/#Color/Color/' /etc/pacman.conf
sed -i "/\[multilib\]/,/Include/"'s/^#//' /etc/pacman.conf
pacman -Sy

# Install microcode
	proc_type=$(lscpu)
	if grep -E "GenuineIntel" <<< ${proc_type}; then
		echo "Installing Intel microcode"
		pacman -S --noconfirm intel-ucode
		proc_ucode=intel-ucode.img
	elif grep -E "AuthenticAMD" <<< ${proc_type}; then
		echo "Installing AND microcode"
		pacman -S --noconfirm amd-ucode
		proc_ucode=amd-ucode.img
	fi

# Install display server and graphics drivers
if [[ -z ${DESKTOP} ]]; then
	pacman -S xorg xorg-xinit mesa lib32-mesa mesa-utils --noconfirm
	gpu_type=$(lspci)
	if grep -E "NVIDIA|GeForce" <<< ${gpu_type}; then
		pacman -S nvidia nvidia-utils lib32-nvidia-utils --noconfirm
	elif grep -E "Radeon|AMD" <<< ${gpu_type}; then
		pacman -S x86-video-amdgpu --noconfirm
	elif grep -E "Integrated Graphics Controller|Intel Corporation UHD" <<< ${gpu_type}; then
		pacman -S xf86-video-intel vulkan-intel lib32-vulkan-intel libva-intel-driver libvdpau-va-gl libva-utils --noconfirm
	fi
fi

# Hostname
echo $NAMEOFMACHINE > /etc/hostname

# Root password
echo "Enter root password"
passwd

# Add user
if [ $(whoami) = "root" ]; then
	pacman -S --noconfirm zsh
	useradd -m -G wheel -s /bin/zsh $USERNAME

	echo "$USERNAME:$PASSWORD" | chpasswd

	cp -R /root/archinstall /home/$USERNAME
	chown -R $USERNAME: /home/$USERNAME/archinstall

else
	echo "You are already a user"
fi
