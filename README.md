# Arch Install System

## Description

This system produces an Arch Linux installation with the QTile window manager as described in my dotfiles @ https://gitlab.com/alk-alyss/dotfiles

## Notes
This system has been tested on my hardware. It is not guarranteed that it will work for your hardware

The packages to be installed are listed in packagelist.txt. Edit it to your preferences

If you are using this system when it asks for PC type choose other
This system is setup for my use case so the desktop and laptop options are used to configure some options and install extra packages located in desktop-packages.txt or laptop-packages.txt depending on the option

## Usage

- Dowload, flash and boot a current ArchISO from https://archlinux.org/download
- Setup the disks as described in the Arch Wiki (this system does not touch the disks in any way) and mount the partition on which the installation will happen onto /mnt, as well as any other necessary partitions (e.g. /boot, /efi, /home)
- From the initial promt type the commands

``` bash
pacman -Sy git
git clone https://gitlab.com/alk-alyss/archinstall
cd archinstall
./archinstall.sh
```

## Credits

Most of this system is carried over from the project ArchTitus of ChrisTitusTech @ https://github.com/ChrisTitusTech/ArchTitus
