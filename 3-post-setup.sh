#!/bin/bash

source /root/archinstall/setup.conf

# Symlink neovim config for root
mkdir /root/.config
ln -s /home/${USERNAME}/.config/nvim /root/.config/nvim

# Symlink gtk config for root
ln -s /home/${USERNAME}/.config/gtk-2.0/gtkrc-2.0 /etc/gtk-2.0/gtkrc
ln -s /home/${USERNAME}/.config/gtk-3.0/settings.ini /etc/gtk-3.0/settings.ini

if [[ -d "/sys/firmware/efi" ]]; then
	pacman -S --noconfirm efibootmgr
	grub-install --target=x86_64-efi --efi-directory=/efi --bootloader-id=GRUB $DISK
else
	grub-install --target=i386-pc $DISK
fi

echo "Installing Tela grub theme"
git clone https://github.com/vinceliuice/grub2-themes
cd grub2-themes
./install.sh -b -t tela
cd ..
rm -rf grub2-themes

# Enable system services
systemctl enable NetworkManager
systemctl enable acpid

if [[ ${DESKTOP} ]]; then
	systemctl enable plexmediaserver
	systemctl enable numLockOnTty
fi

if [[ ${LAPTOP} ]]; then
	systemctl enable bluetooth
fi

# Remove no password sudo rights
sed -i 's/^%wheel ALL=(ALL:ALL) NOPASSWD: ALL/# %wheel ALL=(ALL:ALL) NOPASSWD: ALL/' /etc/sudoers
# Add sudo rights
sed -i 's/^# %wheel ALL=(ALL:ALL) ALL/%wheel ALL=(ALL:ALL) ALL/' /etc/sudoers

rm -r /root/archinstall
rm -r /home/$USERNAME/archinstall
