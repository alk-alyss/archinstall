#!/bin/bash

SCRIPT_DIR="$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &> /dev/null && pwd)"
CONFIG_FILE=$SCRIPT_DIR/setup.conf
if [ ! -f $CONFIG_FILE ]; then
	touch -f $CONFIG_FILE
fi

set_option() {
	if grep -Eq "^${1}.*" $CONFIG_FILE; then
		sed -i -e "/^${1}.*/d" $CONFIG_FILE
	fi
	echo "${1}=${2}" >> $CONFIG_FILE
}

timezone() {
	time_zone="$(curl --fail https://ipapi.co/timezone)"
	echo -e "Detected time zone '$time_zone'"
	read -p "It this correct? [y/n]: " answer
	case $answer in
		y|Y|yes|Yes|YES)
			set_option TIMEZONE $time_zone
			;;
		n|N|no|No|NO)
			read -p "Enter desired timezone (e.g. Europe/London) :" new_timezone
			set_option TIMEZONE $new_timezone
			;;
		*)
			echo "Invalid response"
			clear
			timezone
			;;
	esac
}

keymap() {
echo -ne "
Select keyboard layout from this list
	by
	ca
	cf
	cz
	de
	dk
	es
	et
	fa
	fi
	fr
	gr
	hu
	il
	it
	lt
	lv
	mk
	nl
	no
	pl
	ro
	ru
	sg
	ua
	uk
	us
"
	read -p "Keyboard layout: " keymap
	set_option KEYMAP $keymap
}

userinfo() {
	# Get hostname
	while true
	do
		read -p "Name your machine: " nameofmachine
		# Validate hostname
		if [[ "${nameofmachine,,}" =~ ^[a-z][a-z0-9_.-]{0,62}[a-z0-9]$ ]]
		then
			set_option NAMEOFMACHINE $nameofmachine
			break
		fi

		# If validation fails allow the user to force saving of the hostname
		read -p "Hostname doesn't seem correct. Do you still want to save it? [y/n]" force
		if [[ "${force,,}" = "y" ]]
		then
			set_option NAMEOFMACHINE $nameofmachine
			break
		fi
	done

	# Get username
	while true
	do
		# Read username from input
		read -p "Enter username: " username
		# Validate input
		if [[ "${username,,}" =~ ^[a-z_]([a-z0-9_-]{0,31}|[a-z0-9_-]{0,30}\$)$ ]]
		then
			set_option USERNAME ${username,,}
			break
		fi

		# If input is not valid loop until it is valid
		echo "Invalid username"
	done

	# Get password
	while true
	do
		echo -n "Enter password: "
		read -s password
		echo ""
		echo -n "Confirm password: "
		read -s confirm_password
		echo ""
		if [ $password == $confirm_password ]; then
			set_option PASSWORD $password
			break
		fi

		# If passwords do not match loop until they do
		echo "Passwords do not match try again."

	done
}

ssd() {
	while true
	do
		read -p "Is this an SSD? [y/n]: " answer
		case $answer in
			y|Y|yes|Yes|YES)
				set_option SSD 1
				break
				;;
			n|N|no|No|NO)
				set_option SSD 0
				break
				;;
			*)
				echo "Invalid response"
				;;
		esac
	done
}

disk() {
	lsblk -n --output TYPE,KNAME,SIZE | awk '$1=="disk"{print "/dev/"$2" - "$3}'
	read -p "Enter full path to disk (example /dev/sda): " disk
	set_option DISK $disk

	ssd
}

pctype() {
	while true
	do
		read -p "PC type (option used to install extra stuff) [desktop/laptop/other]: " answer
		case $answer in
			desktop)
				set_option DESKTOP 1
				break
				;;
			laptop)
				set_option LAPTOP 1
				break
				;;
			other)
				break
				;;
			*)
				echo "Invalid response"
				;;
		esac
	done
}

clear
timezone
clear
keymap
clear
disk
clear
userinfo
clear
pctype
clear
