#!/bin/bash

SCRIPT_DIR="$(cd -- "$(dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)"

while true
do
	read -p "Have you created and mounted the partitions? [y/n]: " answer
	case $answer in
		y|Y|yes|Yes|YES)
			echo "Great continuing with install process!"
			break
			;;
		n|N|no|No|NO)
			echo "THEN GO DO THAT!"
			exit
			;;
		*)
			echo "Invalid response"
			;;
	esac
done

bash startup.sh
source $SCRIPT_DIR/setup.conf
bash 0-base-install.sh
arch-chroot /mnt /root/archinstall/1-setup.sh
arch-chroot /mnt /usr/bin/runuser -u $USERNAME -- /home/$USERNAME/archinstall/2-user.sh
arch-chroot /mnt /root/archinstall/3-post-setup.sh
