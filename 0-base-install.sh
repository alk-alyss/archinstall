#!/bin/bash

SCRIPT_DIR="$(cd -- "$(dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)"

# Import configuration
source setup.conf

# Enable time synchronization
timedatectl set-ntp true

# Enable parallel downloads, get best mirrors
sed -i 's/#Para/Para/' /etc/pacman.conf
iso=$(curl -4 ifconfig.co/country-iso)
pacman -S --noconfirm reflector
cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.bak
reflector -a 48 -c $iso -f 5 -l 20 --sort rate --save /etc/pacman.d/mirrorlist

# Install base system
pacstrap /mnt base base-devel linux linux-firmware neovim git networkmanager grub
genfstab -U /mnt >> /mnt/etc/fstab
cp -R ${SCRIPT_DIR} /mnt/root/archinstall
cp /etc/pacman.d/mirrorlist /mnt/etc/pacman.d/mirrorlist
